<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
   <title>Document</title>
   <script src="script.js"></script>
</head>

<body>
<script>
   $(function(){
      $("#provincias_select").change(function () {
      let selectedValue = this.value;
      console.log(selectedValue)
      $.ajax({
         type: "POST",
         url: "search.php",
         data: `search=${selectedValue}`,
         cache: false,
         success: function(html) {
            console.log(html);
            $("#municipios_select").html(html).show()
         }
      })
   });
   })
</script>
   <form id="loginform" method="POST">
      <div>
         <label for="username">Username:</label>
         <input type="text" name="username" id="username">
         <label for="password">Password: </label>
         <input type="password" name="password" id="password">
      </div>
      <button type="submit">Send</button>
   </form>

</body>

</html>