<!DOCTYPE html>
<html lang="en">

   <?php
   $mysqli = new mysqli("mysql", "edu", "edu1234567", "dockerDB");
   ?>
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Document</title>
   <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
</head>

<body>
   <p>You'r logged</p>
   <script>
      $(function() {
         $("#provincias_select").change(function() {
            let selectedValue = this.value;
            $.ajax({
               type: "POST",
               url: "search.php",
               data: `search=${selectedValue}`,
               cache: false,
               success: function(html) {
                  $("#municipios_select").html(html).show()
               }
            })
         });
      })
   </script>
   <div>
      <label for="provincias_select">Provincias:</label>
      <select name="provincias" id="provincias_select">
         <?php
         $result = $mysqli->query("SELECT PROVINCIA FROM MUNICIPIOS GROUP BY provincia");

         while ($row = $result->fetch_array()) { ?>
            <option value="<?= $row["PROVINCIA"] ?>"><?= $row["PROVINCIA"] ?></option>
         <?php } ?>
      </select>

      <label for="municipios_select">Municipios:</label>
      <select name="municipios" id="municipios_select">
      </select>
   </div>
</body>

</html>