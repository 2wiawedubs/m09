$(document).ready(function () {
   $("#loginform").submit(function (e) {
      e.preventDefault();
      $.ajax({
         type: "POST",
         url: "login.php",
         data: $(this).serialize(),
         success: function (response) {
            console.log(response);
            var jsonData = JSON.parse(response);
            console.log(jsonData)
            if (jsonData.success == "1")
               location.href = "dashboard.php"
            else
               alert("invalid Credentials")
         },
         complete: function (param) {
            console.log(param);
         }
      })
   })
   
})
