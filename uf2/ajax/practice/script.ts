const fileName_input = document.getElementById("fileName_input");
const getFile_input = document.getElementById("getFile_input");

const READY_STATE_UNINITIALIZED = 0;
const READY_STATE_LOADING = 1;
const READY_STATE_LOADED = 2;
const READY_STATE_INTERACTIVE = 3;
const READY_STATE_COMPLETE = 4;

getFile_input.addEventListener("click", loadContent(null, "GET", showRequestContent));

function loadContent(url: URL | null, method: string, callFunction): any {
   var http_request: XMLHttpRequest = initXHR() as XMLHttpRequest;

   if (http_request) {
      http_request.onreadystatechange = callFunction(http_request);
      http_request.open(method,
         url == null
            ? `http://localhost/${fileName_input.textContent}`
            : url,
         true);
      http_request.send();
   }
}

function initXHR(): XMLHttpRequest | ActiveXObject {
   if (window.XMLHttpRequest)
      return new XMLHttpRequest;
   else if (window.ActiveXObject)
      return new ActiveXObject("Microsoft.XMLHTTP");
}

function showRequestContent(request: XMLHttpRequest | null) {
   if (request.readyState == READY_STATE_COMPLETE)
      if (request.status == 200)
         console.log(`Request content: ${request.responseText}`);
}

