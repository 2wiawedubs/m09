// 1.Sel·leccionar totes les imatges a la pàgina; registrar en la consola l'atribut alt de cada imagen.
console.log($("img"));
$.each($("img"), function (indexInArray, valueOfElement) { 
    console.log($(valueOfElement).attr("alt"));
});

// 2.Sel·leccionar l'element input, després anar al formulari i afegeix-li una classe.
$.each($("input"), function (indexInArray, valueOfElement) { 
    $(valueOfElement).parents("form").addClass("inputForm");
});

// 3.Sel·leccionar el ítem que té la classe current dins de la llista #myList i el·liminar aquesta classe a l'element; després afegir la classe current al següent ítem de la llista.
$("#myList > .current").removeClass("current");

// 4.Sel·leccionar l'element select dins de #specials; després anar cap al botó submit.
$("#specials select").parents("form").find($("input[type=submit]"))

// 5.Sel·leccionar el primer ítem de la llista en l'element #slideshow; afegir-li la classe current al mateix i  després afegir la classe disabled als elements germans.
$("#slideshow").children().first().addClass("current").siblings().addClass("disabled");

// 6.Afegir 5 nous ítems al final de la llista desordenada #myList. Ajuda: for (var i = 0; i<5; i++) { ... }
let listLgth = $("#myList").children().length;
for (let index = listLgth + 1; index <= listLgth + 5; index++) {
    $("#myList").append("<li>List item" + index + "</li>");
}

// 7.Remoure els ítems impars de la llista.
$("#myList li:even").remove();

// 8.Afegir un altre element h2 i un altre paràgref al últim div.module.
$("div.module:last").append("<h2>Header 2</h2>").append("<p>Paragraph</p>");

// 9.Afegir una altre opció a l' element select; donar-li a l'opció afegida el valor Wednesday.
$("select").append("<option value='Wednesday'>Wednesday</option>");

// 10.Afegir un nou div.module a la pàgina després del últim; després afegir una còpia d'una de les imatges existents dins del nou div.
$("div.module:last").after("<div class='module'></div>")
$("div.module:last").append($("img:last").clone());